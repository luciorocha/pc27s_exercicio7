/**
 * BufferSincronizado: Modelo Produtor/Consumidor
 * 
 * Sem utilizar ArrayBlockingQueue
 * 
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 19/10/2017
 */
package exemplo7;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Principal {

    public Principal(){
        ExecutorService pool = Executors.newCachedThreadPool();
        
        Buffer bufferCompartilhado = new BufferSincronizado();
        
        Produtor t1 = new Produtor(bufferCompartilhado);
        pool.execute ( t1 );
        pool.execute ( new Consumidor(bufferCompartilhado) );
        
        pool.shutdown();
    }
    
    public static void main(String [] args){
        new Principal();
    }
    
}//fim classe
